import React from 'react';

export default class Newsletter extends React.Component{
    render() {
        return(
            <div className="site-footer__item
                    site-footer__item--one-quarter
                    site-footer-newsletter__one-half">
                <div className="site-footer__item-inner site-footer__item-inner--newsletter"><p className="h4">Newsletter</p><div className="site-footer__newsletter
                        "><form method="post" action="/contact#ContactFooter" id="ContactFooter" acceptCharset="UTF-8" className="contact-form" noValidate="novalidate"><input type="hidden" name="form_type" defaultValue="customer" /><input type="hidden" name="utf8" defaultValue="✓" />
                    <input type="hidden" name="contact[tags]" defaultValue="newsletter" />
                    <div className="input-group ">
                        <input type="email" name="contact[email]" id="ContactFooter-email" className="input-group__field newsletter__input" defaultValue placeholder="Email address" aria-label="Email address" aria-required="true" required autoCorrect="off" autoCapitalize="off" />
                    <span className="input-group__btn">
                      <button type="submit" className="btn newsletter__submit" name="commit" id="Subscribe">
                        <span className="newsletter__submit-text--large">Subscribe</span>
                      </button>
                    </span>
                    </div>
                </form>
                </div>
                </div>
            </div>
        )
    }

}