import React from 'react';

export default class Middle extends React.Component{
    render() {
        return(
            <div className="grid-item small--one-whole one-half">
                <h3>Get in touch</h3>
                <p>Use this text to share information about your brand with your customers.</p>
            </div>

        )
    }

}