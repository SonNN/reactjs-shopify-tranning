import React from 'react';

export default class Left extends React.Component{
    render() {
        return(
            <div className="grid-item small--one-whole two-twelfths">
                <h3>Quick links</h3>
                <ul>
                    Pick a link list to show here in your <a href="/admin/themes/74781392995/settings">Theme Settings</a> under <strong>Footer</strong> &gt; <strong>Quick links link list</strong>.
                </ul>
            </div>

        )
    }

}