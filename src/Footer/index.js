import React from 'react';
import FooterHr from '../Footer/Hr';
import Bottom from '../Footer/Bottom';
import Left from '../Footer/Left';
import Middle from '../Footer/Middle';
import Right from '../Footer/Right';

export default class Footer extends React.Component{
    render() {
        return(
            <div id="shopify-section-footer"  className={'site-footer small--text-center'}>
                <footer className={'site-footer'} >
                    <div className={'wrapper'}>
                        <Left />
                        <Middle />
                        <Right />
                        <FooterHr />
                        <Bottom />
                    </div>
                </footer>
            </div>
        )
    }
}