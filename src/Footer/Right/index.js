import React from 'react';

export default class Right extends React.Component{
    render() {
        return(
            <div className="grid-item small--one-whole one-third">
                <h3>Newsletter</h3>
                <div className="site-footer__newsletter-subtext">
                    <p>Promotions, new products and sales. Directly to your inbox.</p>
                </div>
                <div className="form-vertical">
                    <form method="post" action="/contact#contact_form" id="contact_form" acceptCharset="UTF-8" className="contact-form"><input type="hidden" name="form_type" defaultValue="customer" /><input type="hidden" name="utf8" defaultValue="✓" />
                        <input type="hidden" name="contact[tags]" defaultValue="newsletter" />
                        <div className="input-group">
                            <label htmlFor="Email" className="visually-hidden">Email</label>
                            <input type="email" defaultValue placeholder="Email Address" name="contact[email]" id="Email" className="input-group-field" aria-label="Email Address" autoCorrect="off" autoCapitalize="off" />
                            <span className="input-group-btn">
          <button type="submit" className="btn-secondary btn--small" name="commit" id="subscribe">Sign Up</button>
        </span>
                        </div>
                    </form>
                </div>
            </div>

        )
    }

}