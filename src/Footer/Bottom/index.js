import React from 'react';

export default class Bottom extends React.Component{
    render() {
        return(
            <div className="grid">
                <div className="grid-item">
                    <ul className="legal-links inline-list">
                        <li>
                            © 2019 <a href="/" title={"13"}>Smart ReactJS Training</a>
                        </li>
                        <li>
                            <a target="_blank" title={"Footer"} rel="nofollow" href="https://www.shopify.com?utm_campaign=poweredby&utm_medium=shopify&utm_source=onlinestore">Powered by Shopify</a>
                        </li>
                    </ul>
                </div>
            </div>

        )
    }

}