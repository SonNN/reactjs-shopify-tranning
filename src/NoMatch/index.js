import React from 'react';
import MasterPage from '../MasterPage';

export default class NoMatch extends MasterPage{
    _renderBody = () => {
        return (
            <div className="grid">
                <div className="grid-item large--two-thirds push--large--one-sixth text-center">
                    <h1>404 Page Not Found</h1>
                    <p>The page you requested does not exist. Click <a href="/collections/all">here</a> to continue shopping.</p>
                </div>
            </div>
        )
    };
}
