import React from 'react';

export default class Logo extends React.Component{
    render() {
        return(
            <div className={'grid-item large--one-half'}>
                <h1 className="header-logo" itemScope itemType="http://schema.org/Organization">
                    <a href="/" itemProp="url">Smart ReactJS Training</a>
                </h1>
            </div>
        )
    }

}