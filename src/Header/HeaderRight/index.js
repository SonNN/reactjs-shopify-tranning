import React from 'react';
import Search from '../HeaderRight/Search';
import MiniCart from '../HeaderRight/MiniCart';

export default class HeaderRight extends React.Component{
    render() {
        return(
            <div className={'grid-item large--one-half text-center large--text-right'}>
                <div className={'site-header__icons-wrapper'}>
                    <Search />
                    <MiniCart />
                </div>
            </div>
        )
    }

}