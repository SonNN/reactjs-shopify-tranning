import React from 'react';

export default class Search extends React.Component{
    render() {
        return(
            <form action="/search" method="get" className="search-bar" role="search">
                <input type="hidden" name="type" defaultValue="product" />
                <input type="search" name="q" placeholder="Search all products..." aria-label="Search all products..." />
                <button type="submit" className="search-bar--submit icon-fallback-text">
                    <span className="icon icon-search" aria-hidden="true" />
                    <span className="fallback-text">Search</span>
                </button>
            </form>
        )
    }
}