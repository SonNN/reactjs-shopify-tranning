import React from 'react';

export default class MiniCart extends React.Component{
    render() {
        return(
            <a href="/cart" className="header-cart-btn cart-toggle">
                <span className="icon icon-cart" />
                Cart <span className="cart-count cart-badge--desktop hidden-count">0</span>
            </a>
        )
    }
}