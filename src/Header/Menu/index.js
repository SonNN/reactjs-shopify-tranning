import React from 'react';
import {Link} from "react-router-dom";

export default class Menu extends React.Component{

    constructor(props){
        super(props)
    }

    menuList = [
        {title: 'Home', url: '/home'},
        {title: 'Catalog', url: '/catalog'},
        {title: 'LifeStyle', url: '/lifestyle'},
        {title: 'News', url: '/news'},
        {title: 'About', url: '/about'},

    ];

    renderMenu = () => {
        return (
            this.menuList.map(function(item, index){
               return (

                     <li  key={index}>
                         <Link to={item.url} className="site-nav__link site-nav__link--main">{item.title}</Link>
                         {/* <li key={index}>
                        <a href={item.url} className="site-nav__link site-nav__link--main">
                            <span className="site-nav__label">{item.title}</span>
                        </a>
                    </li>*/  }
                     </li>



               )

            })

        );
    };

    render() {
        return(
            <nav className="nav-bar" id="navBar" role="navigation">
                <div className="wrapper">
                    <form action="/search" method="get" className="search-bar" role="search">
                        <input type="hidden" name="type" defaultValue="product" />
                        <input type="search" name="q" defaultValue placeholder="Search all products..." aria-label="Search all products..." />
                        <button type="submit" className="search-bar--submit icon-fallback-text">
                            <span className="icon icon-search" aria-hidden="true" />
                            <span className="fallback-text">Search</span>
                        </button>
                    </form>
                    <ul className="mobile-nav" id="MobileNav">
                        {this.renderMenu()}
                    </ul>
                    <ul className="site-nav" id="AccessibleNav">
                        {this.renderMenu()}
                    </ul>
                </div>
            </nav>
        )
    }
}