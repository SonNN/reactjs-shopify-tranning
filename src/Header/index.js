import React from 'react';
import Logo from '../Header/Logo';
import HeaderRight from '../Header/HeaderRight';

export default class Header extends React.Component{
    render() {
        return(
            <header className={'site-header'}>
                <div className={'wrapper'}>
                    <div className={'grid--full'}>
                        <Logo />
                        <HeaderRight />
                    </div>
                </div>
            </header>
        )
    }
}
