import React from 'react';
import {Link} from "react-router-dom";
import MasterPage from '../MasterPage';
import {Query} from "./Query";
import {Api} from "../Api";

export default class Catalog extends MasterPage {

    constructor(props) {
        super(props);
        this.state = {
            productsCollection: null
        }
    }

    componentDidMount() {

        const api = Api();
        const query = Query();

        api.post(null, query).then(response => {
            //console.log('response', response);
            //console.log('data', response.data);
            if (response && response.data) {
                this.setState({
                    productsCollection: response.data.data.products.edges
                });
            }
        }).catch(err =>{
            console.log(err);
        });
    }

    _renderProductList = () => {
        const {productsCollection} = this.state;

        if (productsCollection) {
            //console.log(productsCollection);
            return (
                productsCollection.map((product, index) => {
                    return this._renderProduct(product, index);
                })
            );
        }
    };

    _renderProduct = (product, index) => {
        let productDetail = product.node;
        let productImages = productDetail.images.edges;
        let productImage = null;
        productImages.map((image, index) => {
            productImage = image.node.originalSrc;
        });

        return (
            <div key={index} className={'grid-item small--one-half medium--one-quarter large--one-quarter'}>
                <Link to={"/product/" + productDetail.id} className={'product-grid-item'}>
                    <div className={'product-grid-image'}  style={{height: '227px'}}>
                        <div className={'lazyload__image-wrapper no-js'}>
                            <img src={productImage} alt={productDetail.title} className={'no-js lazyautosizes lazyloaded'} />
                        </div>
                    </div>
                    <p>{productDetail.title}</p>
                    <div className={'product-item--price'}>
                        <span className={'h1 medium--left'}>
                            <small>
                                {productDetail.priceRange.minVariantPrice.amount}
                                {productDetail.priceRange.minVariantPrice.currencyCode}
                            </small>
                        </span>
                    </div>
                </Link>
            </div>
        );
    };

    _renderBody = () => {
        return (
            <div>
                <nav className="breadcrumb" role="navigation" aria-label="breadcrumbs">
                    <a href="/" title="Back to the frontpage">Home</a>
                    <span className="divider" aria-hidden="true">›</span>
                    <span>Products</span>
                </nav>
                <div className={'grid-item'}>
                    <header className="section-header">
                        <h1 className="section-header--title section-header--left h1">Products</h1>
                        <div className="section-header--right">
                            <div className="form-horizontal">
                                <label htmlFor="sortBy" className="small--hide">Sort by</label>
                                <select name="sort_by" aria-describedby="a11y-refresh-page-message" id="sortBy">
                                    <option value="manual">Featured</option>
                                    <option value="best-selling">Best selling</option>
                                    <option value="title-ascending" selected="selected">Alphabetically, A-Z</option>
                                    <option value="title-descending">Alphabetically, Z-A</option>
                                    <option value="price-ascending">Price, low to high</option>
                                    <option value="price-descending">Price, high to low</option>
                                    <option value="created-ascending">Date, old to new</option>
                                    <option value="created-descending">Date, new to old</option>
                                </select>
                            </div>
                            <div className="collection-view">
                                <a title="Grid view" className="change-view collection-view--active" data-view="grid">
                                    <span className="icon icon-collection-view-grid" />
                                </a>
                                <a title="List view" className="change-view" data-view="list">
                                    <span className="icon icon-collection-view-list" />
                                </a>
                            </div>
                            <button id="toggleFilters" className="btn btn--small right toggle-filters">Filters</button>
                        </div>
                    </header>

                    <div className={'grid-uniform'}>
                        {this._renderProductList()}
                    </div>
                </div>
            </div>
        )
    };
}
