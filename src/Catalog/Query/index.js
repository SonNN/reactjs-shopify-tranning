export function Query () {
    return {
        query: `{
            products(first: 100) {
                edges {
                    node {
                        id
                        title
                        description
                        descriptionHtml
                        priceRange {
                            minVariantPrice {
                                amount
                                currencyCode
                            }
                            maxVariantPrice {
                                amount
                                currencyCode
                            }
                        } 
                        images(first: 1) {
                            edges {
                                node {
                                    altText
                                    originalSrc                                        
                                }
                            }
                        }
                    }
                }
            }
        }`
    };
}
