export default class ProductModel {

    _id;
    _name;
    _image;
    _price;

    constructor (_id, _name, _image, _price) {
        this._id = _id;
        this._name = _name;
        this._image = _image;
        this._price = _price;
    }
}
