import React from 'react';
import Header from "../Header";
import Footer from "../Footer";
import Menu from "../Header/Menu";

export default class MasterPage extends React.Component{

    _renderHeader = () => {
        return (
            <Header />

        )
    };

    _renderMenu = () => {
        return (
            <Menu />
        )
    };

    _renderBody = () => {
        return (
            null
        )
    };

    _renderFooter = () => {
        return (
            <Footer />
        )
    };

    render() {
        return(
            <div className="container supports-fontface">
                {this._renderHeader()}
                {this._renderMenu()}
                <div className={'wrapper main-content'}>
                    {this._renderBody()}
                </div>
                {this._renderFooter()}
            </div>
        )
    }

}