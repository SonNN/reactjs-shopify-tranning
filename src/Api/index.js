import axios from 'axios';

export function Api () {
    const BASE_URL = `https://smart-reactjs-training.myshopify.com/api/graphql.json`;
    const ShopifyHeaderValue = 'b8fec450096b1daba7d3e1e61511553e';

    return axios.create({
        baseURL: BASE_URL,
        headers: {
            'X-Shopify-Storefront-Access-Token': ShopifyHeaderValue,
            'Content-Type': 'application/json',
        }
    });
}
