import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import HomePage from '../HomePage'
import Catalog from '../Catalog'
import Product from '../Product'
import NoMatch from '../NoMatch'

export default class Routes extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Route path={'/'} exact component={HomePage} />
                <Route path={'/home'} exact component={HomePage} />
                <Route path={'/catalog'} exact component={Catalog} />
                <Route path={'/product/:id'} component={Product} />
                <Route path='*' component={NoMatch} />
            </BrowserRouter>

        )
    }
}