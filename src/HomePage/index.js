import React from 'react';
import MasterPage from '../MasterPage';
import HomeSlider from '../HomePage/Slider'
import FeatureCollection from '../HomePage/FeatureCollection'

export default class HomePage extends MasterPage{

    _renderBody = () => {
        return (
            <div>
                <HomeSlider />
                <FeatureCollection />
            </div>
        )
    };

}