import React from 'react';
import Slider from "react-slick";

export default class HomeSlider extends React.Component{
    render() {
        let settings = {
            dots: true,
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        };
        return (
            <Slider {...settings}>
                <div className={'slide'}>
                    <img src={'/images/slide_1_1024x_1296x.jpg'} />
                </div>
                <div className={'slide'}>
                    <img src={'/images/slide_2_1024x_1296x.jpg'} />
                </div>
            </Slider>
        );
    }

}