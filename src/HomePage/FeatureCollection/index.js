import React from 'react';
import ProductModel from '../../Models/ProductModel';

export default class FeatureCollection extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            productList: null
        }
    }

    _createProductsData = () => {
        const product1 = new ProductModel(1, 'Product 1', 'https://i.ytimg.com/vi/V5M2WZiAy6k/maxresdefault.jpg', 1000);
        const product2 = new ProductModel(1, 'Product 1', 'https://i.ytimg.com/vi/V5M2WZiAy6k/maxresdefault.jpg', 1000);

        return [product1, product2];
    };

    _renderProductList = () => {
        const {productList} = this.state;
        return (
            <ul>
                {
                    productList.map((item, index) => {
                        this._renderProduct(item);
                    })
                };
            </ul>
        );
    };

    _renderProduct = (product) => {
        return;
    }

    componentDidMount() {
        this.setState({
            productList: this._createProductsData()
        })
    }

    render() {

        return(
            <div>FeatureCollection</div>
        )
    }

}