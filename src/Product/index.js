import React from "react";
import MasterPage from "../MasterPage";
import {Api} from "../Api";

export default class Product extends MasterPage {

    constructor(props) {
        super(props);
        this.state = {
            product: null
        }

    }

    componentDidMount() {

        const api = Api();
        const {id} = this.props.match.params;
        const query1 =  {
            "query": "{" +
                "product(id: \"" + id + "\") {" +
                    "title" +
                    "description" +
                    "}" +
                "}"
            };

        const query2 = {
            "query": "{node(id: \"" + id + "\") { id ... on Product { title description images(first:10) { edges {" +
                " node { originalSrc } } } variants(first: 10) { edges { node { id sku price selectedOptions { name value } } } } } } }"
        };


        api.post(null, query2).then(response => {
            //console.log("response", response);
            //console.log("data", response.data.data.node);
            if (response && response.data.data.node) {
                this.setState({
                    product: response.data.data.node
                });
            }
        }).catch(err =>{
            console.log(err);
        });
    }

    _renderProductImage = (product) => {
            let productImage = null;
            product.images.edges.map((image, index) => {
                productImage = image.node.originalSrc;
            });

            return (
                <div className={"grid-item large--two-fifths"}>
                    <div id={"productPhotoContainer-product-template"} className={"product-photo-container"}>
                        <div className={"lazyload__image-wrapper"}>
                            <div className={"no-js product__image-wrapper"}>
                                <img src={productImage} alt={product.title} />
                            </div>
                        </div>
                    </div>
                </div>
            );

    };

    _renderProductDetails = (product) => {
        return (
            <div className={"grid-item large--three-fifths"}>
                <h1 className="h2" itemProp="name">{product.title}</h1>
                <div itemProp="offers" itemScope itemType="http://schema.org/Offer">
                    <ul className="inline-list product-meta">
                        <li>
                            <span id="productPrice-product-template" className="h1">
                                <span>{product.variants.edges[0].node.price}</span></span>
                        </li>
                    </ul>
                    <hr id="variantBreak" className="hr--clear hr--small"/>
                    <link itemProp="availability" href="http://schema.org/InStock"/>
                    <form method="post" action="/cart/add" id="addToCartForm-product-template" acceptCharset="UTF-8"
                          className="addToCartForm addToCartForm--payment-button
" encType="multipart/form-data"><input type="hidden" name="form_type" defaultValue="product"/><input type="hidden"
                                                                                                     name="utf8"
                                                                                                     defaultValue="✓"/>
                        <div className="selector-wrapper" style={{display: "none"}}><select
                            className="single-option-selector" data-option="option1"
                            id="productSelect-product-template-option-0">
                            <option value="Default Title">Default Title</option>
                        </select></div>
                        <div className="payment-buttons payment-buttons--small">
                            <button type="submit" name="add" id="addToCart-product-template"
                                    className="btn btn--add-to-cart btn--secondary-accent">
                                <span className="icon icon-cart"/>
                                <span id="addToCartText-product-template">Add to Cart</span>
                            </button>
                        </div>
                    </form>
                    <hr className="product-template-hr"/>
                </div>

                <div className="product-description rte" itemProp="description">
                    <meta charSet="utf-8" /><span>{product.description}</span>
                </div>

            </div>
        );
    };

    _renderBody = () => {
        if (this.state && this.state.product) {
            const {product} = this.state;
            console.log(product);
            return (
                <div id={"shopify-section-product-template"} className={"shopify-section product-template-section"}>
                    <div id={"ProductSection"}>
                        <div className={"grid"}>
                            {this._renderProductImage(product)}
                            {this._renderProductDetails(product)}
                        </div>
                    </div>
                </div>
            )
        }
    }
}